-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2021 at 02:01 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lnuthesisrepodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblauthors`
--

CREATE TABLE `tblauthors` (
  `AuthorId` int(30) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Mname` varchar(50) DEFAULT NULL,
  `ThesisId` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblautonumbers`
--

CREATE TABLE `tblautonumbers` (
  `AUTOID` int(11) NOT NULL,
  `AUTOSTART` varchar(30) NOT NULL,
  `AUTOEND` int(11) NOT NULL,
  `AUTOINC` int(11) NOT NULL,
  `AUTOKEY` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblautonumbers`
--

INSERT INTO `tblautonumbers` (`AUTOID`, `AUTOSTART`, `AUTOEND`, `AUTOINC`, `AUTOKEY`) VALUES
(1, '000', 20, 1, 'BorrowerID');

-- --------------------------------------------------------

--
-- Table structure for table `tblbooknumber`
--

CREATE TABLE `tblbooknumber` (
  `ID` int(11) NOT NULL,
  `BOOKTITLE` varchar(255) NOT NULL,
  `QTY` int(11) NOT NULL,
  `Desc` varchar(90) NOT NULL,
  `Author` varchar(90) NOT NULL,
  `PublishDate` date NOT NULL,
  `Publisher` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbooknumber`
--

INSERT INTO `tblbooknumber` (`ID`, `BOOKTITLE`, `QTY`, `Desc`, `Author`, `PublishDate`, `Publisher`) VALUES
(5, 'life of juan', 3, 'life of juan', 'unknown', '2016-10-10', 'unknown'),
(6, 'the computerizez system', 2, 'computer', 'unknown', '2016-10-10', 'unknown'),
(7, 'language of us', 1, 'language', 'unknown', '2016-10-10', 'unknown'),
(8, 'science', 1, 'invention of science', 'unknown', '2016-10-10', 'unknown'),
(9, 'book', 3, 'book revised', 'unknown', '2016-10-10', 'unknown'),
(10, 'the only book', 1, 'book', 'unknown', '2016-10-10', 'uknown'),
(11, 'book  now', 2, 'book', 'unknown', '2016-10-10', 'unknown'),
(12, 'the one', 1, 'book1', 'unknown', '2016-10-10', 'unknown'),
(13, 'the life of june', 2, 'journey', 'unknown', '2016-10-10', 'unknown'),
(14, 'title', 1, 'book', 'unknown', '2016-10-10', 'unknown'),
(15, 'sad', 1, 's', 'da', '2018-03-25', 'as'),
(16, '2wqe', 1000, 'wqe', 'wqe', '2018-03-25', 'wqe');

-- --------------------------------------------------------

--
-- Table structure for table `tblbooks`
--

CREATE TABLE `tblbooks` (
  `BookID` int(11) NOT NULL,
  `IBSN` varchar(13) NOT NULL,
  `BookTitle` varchar(125) NOT NULL,
  `BookDesc` varchar(255) NOT NULL,
  `Author` varchar(125) NOT NULL,
  `PublishDate` date NOT NULL,
  `BookPublisher` varchar(125) NOT NULL,
  `Category` varchar(90) NOT NULL,
  `Program` varchar(30) NOT NULL,
  `BookPrice` double NOT NULL,
  `BookQuantity` int(11) NOT NULL,
  `Status` varchar(30) NOT NULL,
  `BookType` varchar(90) NOT NULL,
  `DeweyDecimal` varchar(90) NOT NULL,
  `OverAllQty` int(11) NOT NULL,
  `Donate` tinyint(1) NOT NULL,
  `Remarks` varchar(90) DEFAULT NULL,
  `FileLocation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbooks`
--

INSERT INTO `tblbooks` (`BookID`, `IBSN`, `BookTitle`, `BookDesc`, `Author`, `PublishDate`, `BookPublisher`, `Category`, `Program`, `BookPrice`, `BookQuantity`, `Status`, `BookType`, `DeweyDecimal`, `OverAllQty`, `Donate`, `Remarks`, `FileLocation`) VALUES
(1, '12345678', 'life of juan', 'life of jose and maria', 'unknown', '2016-10-10', 'unknown', 'History and Geography', 'AB POL SCI', 175, 1, 'Available', 'Fiction', '900', 1, 0, 'Donate', ''),
(6, '1345673', 'Looking into the Different Perceptions of Teachers upon the Pending Removal of the Filipino Subject', 'The purpose of this study is to delve into the teacherâ€™s perception on the pending removal of the Filipino and Panitikan as a core subject in college among the teachers working within Quezon City with objectives of determining the standpoint of teachers', 'Mark Santos', '2016-10-10', 'Jimmy Santos', 'Literature', 'AB ENGLISH', 0, 1, 'Available', 'Fiction', '800', 1, 0, 'for Filipino students', ''),
(7, '14256372', 'Development of polyethersulfone phase-inversion membranes for membrane distillation using oleophobic coatings', 'Highly porous macrovoid-free polyethersulfone membranes have been prepared using the phase-inversion process with water as the non-solvent. These membranes are of great interest for membrane distillation (MD) after application of a hydrophobic/oleophobic ', 'Janno Palacios', '2016-10-10', 'Albertson Einstien', 'Science', 'BSBIO', 0, 1, 'Available', 'Fiction', '500', 1, 0, 'biology majors', ''),
(18, '12345670', 'Making Southeast Asian Migrant Workers Visible in Taiwanese Cinema: Pinoy Sunday and Ye-Zai', 'In Taiwan, the term for \"migrant workers\" (waiji yigong) refers to non-Han immigrant populationsâ€”including those from Thailand and the Philippinesâ€”whose numbers have been increasing since the 2000s. As these populations have grown, they have become pa', 'Janno Palacios', '2016-10-10', 'Betty De Lapaz', 'Arts and Recreation', 'BEED-SI', 0, 1, 'Available', 'Fiction', '700', 1, 0, 'for artists', ''),
(19, '12345677', 'life of juan', 'life of juan and jose', 'unknown', '2016-10-10', 'unknown', 'History and Geography', 'AB POL SCI', 900, 1, 'Available', 'Fiction', '900', 1, 0, 'Donate', ''),
(52, '202110020', 'Development of Web-based Thesis Repository for Leyte Normal University using Bayesian Algorithm', 'Students of Leyte Normal University are required to have a thesis book as a pre-requisite for graduation. Those students who successfully pass the subject and who finished their research paper are mandated to submit three hardbound book copies of their re', 'Paul Christian Albia', '2021-03-03', 'Jericho D. Amando', 'Computers, Information and General Reference', 'BSIT', 0, 1, 'Available', '', '000', 1, 0, 'this is free', 'uploaded_files/chapter1 Online Repository.docx'),
(62, '202110034', 'Silhouette Guessing Game', '             Students look at the black-and-white silhouettes without objects and try to guess what is that silhouete having different categories such as animals, people or places. When they choose the correct option, the object show up to confirm their g', 'Janno Palacios', '1131-02-11', 'Richard Gutierrez', 'Technology', 'BSIT', 0, 1, 'Available', '', '600', 1, 0, 'this is free', 'uploaded_files/Nowadays.docx'),
(70, '123456789', 'tryla gad ini', 'zdffasfsdfsdfsdfsfsdf', 'tian', '2021-06-03', '', 'Computers, Information and General Reference', '', 0, 1, 'Available', '', '000', 1, 0, 'asdasdsa', 'uploaded_files/prof ethics.docx');

-- --------------------------------------------------------

--
-- Table structure for table `tblcategory`
--

CREATE TABLE `tblcategory` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(125) NOT NULL,
  `DDecimal` varchar(90) NOT NULL,
  `Course` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcategory`
--

INSERT INTO `tblcategory` (`CategoryId`, `Category`, `DDecimal`, `Course`) VALUES
(1, 'Computers, Information and General Reference', '000', 'BS Information Technology'),
(2, 'Philosophy and Psychology', '100', 'BS Social Science'),
(3, 'Religion', '200', ''),
(4, 'Social Science', '300', ''),
(5, 'Language', '400', 'AB English'),
(6, 'Science', '500', 'BS Information Technology'),
(7, 'Technology', '600', 'BS Information Technology'),
(8, 'Arts and Recreation', '700', ''),
(9, 'Literature', '800', ''),
(10, 'History and Geography', '900', ''),
(12, 'ALL', 'ALL', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblinstructors`
--

CREATE TABLE `tblinstructors` (
  `IDNO` int(11) NOT NULL,
  `FNAME` varchar(125) NOT NULL,
  `MNAME` varchar(125) DEFAULT NULL,
  `LNAME` varchar(125) NOT NULL,
  `EMAIL` varchar(125) NOT NULL,
  `Category` varchar(30) NOT NULL,
  `PASS` varchar(255) NOT NULL,
  `ITYPE` varchar(125) NOT NULL,
  `Status` varchar(30) NOT NULL,
  `PicLoc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstructors`
--

INSERT INTO `tblinstructors` (`IDNO`, `FNAME`, `MNAME`, `LNAME`, `EMAIL`, `Category`, `PASS`, `ITYPE`, `Status`, `PicLoc`) VALUES
(1, 'Hazel-Ivy', NULL, 'Acebuche', 'hazellnu@gmail.com', '600', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Instructor', 'Active', ''),
(2, 'Cheenee', 'C', 'Bico', 'cheenee@gmail.com', '400', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Instructor', 'Active', ''),
(3, 'Sonia', 'R', 'Albia', 'sonia@gmail.com', '', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Instructor', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogs`
--

CREATE TABLE `tbllogs` (
  `LogId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `LogDate` datetime NOT NULL,
  `LogMode` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbllogs`
--

INSERT INTO `tbllogs` (`LogId`, `UserId`, `LogDate`, `LogMode`) VALUES
(1, 3, '2016-08-22 14:30:29', 'Logged in'),
(2, 3, '2016-08-22 14:38:37', 'Logged in'),
(3, 3, '2016-08-22 14:39:03', 'Logged out'),
(4, 3, '2016-08-22 14:47:10', 'Logged in'),
(5, 3, '2016-08-22 14:48:08', 'Logged in');

-- --------------------------------------------------------

--
-- Table structure for table `tblpayment`
--

CREATE TABLE `tblpayment` (
  `PaymentId` int(11) NOT NULL,
  `BorrowId` int(11) NOT NULL,
  `Payment` double NOT NULL,
  `Change` double NOT NULL,
  `DatePayed` date NOT NULL,
  `BorrowerId` int(11) NOT NULL,
  `Remarks` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpayment`
--

INSERT INTO `tblpayment` (`PaymentId`, `BorrowId`, `Payment`, `Change`, `DatePayed`, `BorrowerId`, `Remarks`) VALUES
(1, 12345673, 130, 0, '2016-11-22', 912, 'Settled'),
(2, 12345673, 4, 0, '2016-11-22', 912, 'Settled'),
(3, 12345671, 4, 0, '2016-11-22', 912, 'Settled'),
(4, 18293746, 37, 13, '2018-02-09', 912, 'Settled'),
(5, 1345672, 37, 3, '2018-02-09', 912, 'Settled'),
(6, 1345673, 1200, 100, '2018-03-26', 1234, 'Settled');

-- --------------------------------------------------------

--
-- Table structure for table `tblprograms`
--

CREATE TABLE `tblprograms` (
  `id` int(11) NOT NULL,
  `ProgramName` varchar(50) NOT NULL,
  `CollegeNo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblprograms`
--

INSERT INTO `tblprograms` (`id`, `ProgramName`, `CollegeNo`) VALUES
(1, 'ABCOM', 2),
(2, 'AB POL SCI', 2),
(3, 'AB ENGLISH', 1),
(4, 'BEED', 1),
(5, 'BEED/BSED', 1),
(6, 'BEED-SI', 1),
(7, 'BSED', 1),
(8, 'BSHAE', 3),
(9, 'BSHRM', 3),
(10, 'BSSW', 2),
(11, 'BSTHRM', 3),
(12, 'BSBIO', 1),
(13, 'BSIT', 2),
(14, 'BSMATH', 2),
(15, 'BLIS', 1),
(16, 'Bachelor of Science in Tourism Management (BSTM)', 3),
(17, 'Bachelor of Science in Hospitality Management (BSH', 3),
(18, 'Bachelor of Science in Entrepreneurship (BSEntrep)', 3),
(19, 'TCP', 3),
(20, 'Secondary Grade VII', 1),
(21, 'ELementary Grade I-VI', 1),
(22, 'Pre-Elementary', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

CREATE TABLE `tblstudent` (
  `IDNO` int(11) NOT NULL,
  `StudentId` varchar(90) NOT NULL,
  `Firstname` varchar(125) NOT NULL,
  `Lastname` varchar(125) NOT NULL,
  `MiddleName` varchar(125) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Sex` varchar(11) NOT NULL,
  `ContactNo` varchar(125) NOT NULL,
  `CourseYear` varchar(125) NOT NULL,
  `StudentPhoto` varchar(255) NOT NULL,
  `StudentType` varchar(35) NOT NULL,
  `Stats` varchar(36) NOT NULL,
  `BUsername` varchar(90) NOT NULL,
  `BPassword` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`IDNO`, `StudentId`, `Firstname`, `Lastname`, `MiddleName`, `Address`, `Sex`, `ContactNo`, `CourseYear`, `StudentPhoto`, `StudentType`, `Stats`, `BUsername`, `BPassword`) VALUES
(2, '213', 'Janno', 'Palacios', 'E', 'kabankalan City', 'Male', '0192836383', 'BS Information Technology', 'Chrysanthemum.jpg', 'Student', 'Active', '', ''),
(7, '9213', 'Marilou', 'Avelez', 'Lotera', 'Brgy. Patrimony Village Villaba, Leyte', 'Female', '09305678456', 'BS Information Technology', 'Lighthouse.jpg', 'Students', 'Active', 'Marilou123', ''),
(9, '123432', 'Mark', 'Santos', 'Entrada', 'Brgy. 90 Sirado McArthur, Leyte', 'Male', '09291918272', 'AB English', 'student/16062021073941Butterfly-PNG-Photo.png', 'Students', 'Active', 'Mark123', ''),
(23, '202000016', 'Jean', 'Sy', 'Ong', 'Philippines', 'Female', '0923564521', 'AB English', 'student/10062021094217luffychibi.jpg', 'Students', 'Active', 'Jean', '8c34f90c4d920b3126a0a0bf4aecf2a820fce8a7'),
(24, '202100017', 'Paul Christian', 'Albia', 'Rosales', 'Brgy. 96 Calanipawan Tacloban City, Leyte', 'Male', '090909090909', 'BS Information Technology', 'borrower/28052021072527business-minded-people.jpg', 'Students', 'Active', 'mono_paully', '8cb2237d0679ca88db6464eac60da96345513964'),
(25, '202100018', 'Mary Joy', 'Nabartey', 'Campo', 'San Jose, Tacloban City', 'Female', '09282134567', 'BS Information Technology', 'student/15062021065415business-minded-people.jpg', 'Students', 'Active', 'MaryJoy123', '204036a1ef6e7360e536300ea78c6aeb4a9333dd'),
(30, '202100019', 'Jomar', 'Salazar', 'Gutierrez', 'Brgy. 45 Manlurip Tacloban City, Leyte', 'Male', '09098756436', 'BS Information Technology', 'student/16062021073511studenticon.png', 'Students', 'Active', 'Jomar123', '204036a1ef6e7360e536300ea78c6aeb4a9333dd'),
(31, '202100020', 'Gary Jade', 'Basan', 'Trillo', 'Brgy. 78 Cabangkalan Daram, Samar', 'Male', '097658964', 'BS Information Technology', 'student/', 'Students', 'Active', 'Gary123', '204036a1ef6e7360e536300ea78c6aeb4a9333dd');

-- --------------------------------------------------------

--
-- Table structure for table `tbltransaction`
--

CREATE TABLE `tbltransaction` (
  `TransactionID` int(11) NOT NULL,
  `IBSN` varchar(13) NOT NULL,
  `NoCopies` int(11) NOT NULL,
  `DateBorrowed` datetime NOT NULL,
  `Purpose` varchar(90) NOT NULL,
  `Status` varchar(30) NOT NULL,
  `DueDate` datetime NOT NULL,
  `StudentId` int(11) NOT NULL,
  `Borrowed` tinyint(1) NOT NULL,
  `Due` tinyint(1) NOT NULL,
  `Returned` tinyint(1) NOT NULL,
  `DateReturned` datetime NOT NULL,
  `Remarks` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltransaction`
--

INSERT INTO `tbltransaction` (`TransactionID`, `IBSN`, `NoCopies`, `DateBorrowed`, `Purpose`, `Status`, `DueDate`, `StudentId`, `Borrowed`, `Due`, `Returned`, `DateReturned`, `Remarks`) VALUES
(64, '2020', 1, '2021-06-13 00:00:00', '1 week', 'Cancelled', '2021-06-20 09:43:00', 202000016, 0, 0, 0, '2021-06-20 00:00:00', 'Cancelled'),
(65, '12345671', 1, '2021-06-13 09:46:00', '1 week', 'Returned', '2021-06-20 09:46:00', 202000016, 0, 0, 1, '2021-06-13 10:16:00', 'Ontime'),
(66, '9876547', 1, '2021-06-14 00:00:00', '1 week', 'Returned', '2021-06-20 10:48:00', 202000016, 0, 0, 1, '2021-06-14 09:35:00', 'Ontime'),
(67, '9876547', 1, '2021-06-14 09:48:00', '1 week', 'Returned', '2021-06-21 09:48:00', 202000016, 0, 0, 1, '2021-06-14 09:48:00', 'Ontime'),
(68, '12345672', 1, '2021-06-14 10:06:00', '1 week', 'Returned', '2021-06-21 10:06:00', 202000016, 0, 0, 1, '2021-06-14 10:09:00', 'Ontime'),
(69, '2020', 1, '2021-06-14 11:51:00', '1 week', 'Cancelled', '2021-06-21 11:51:00', 202100017, 0, 0, 0, '2021-06-21 00:00:00', 'Cancelled'),
(70, '2020', 1, '2021-06-14 12:11:00', '1 week', 'Cancelled', '2021-06-21 12:11:00', 202100017, 0, 0, 0, '2021-06-21 00:00:00', 'Cancelled'),
(71, '2020', 1, '2021-06-14 12:11:00', '1 week', 'Returned', '2021-06-21 12:11:00', 202000016, 0, 0, 1, '2021-06-14 12:49:00', 'Ontime'),
(72, '12345673', 1, '2021-06-14 12:28:00', '1 week', 'Cancelled', '2021-06-21 12:28:00', 202000016, 0, 0, 0, '2021-06-21 00:00:00', 'Cancelled'),
(73, '2020', 1, '2021-06-14 12:42:00', '1 week', 'Returned', '2021-06-21 12:42:00', 202000016, 0, 0, 1, '2021-06-14 12:49:00', 'Ontime'),
(74, '2020', 1, '2021-06-14 12:43:00', '1 week', 'Returned', '2021-06-21 12:43:00', 202100017, 0, 0, 1, '2021-06-14 12:49:00', 'Ontime'),
(75, '2020', 1, '2021-06-14 00:00:00', '1 week', 'Returned', '2021-06-21 12:44:00', 202000016, 0, 0, 1, '2021-06-14 12:49:00', 'Ontime'),
(76, '2020', 1, '2021-06-14 00:00:00', '1 week', 'Returned', '2021-06-21 02:11:00', 202000016, 0, 0, 1, '2021-06-14 02:18:00', 'Ontime'),
(77, '12345671', 1, '2021-06-14 02:12:00', '1 week', 'Cancelled', '2021-06-21 02:12:00', 202000016, 0, 0, 0, '2021-06-21 00:00:00', 'Cancelled'),
(78, '15243678', 1, '2021-06-14 02:18:00', '1 week', 'Returned', '2021-06-21 02:18:00', 202000016, 0, 0, 1, '2021-06-14 11:24:00', 'Ontime'),
(79, '2020', 1, '2021-06-15 02:07:00', '1 week', 'Returned', '2021-06-22 02:07:00', 202100018, 0, 0, 1, '2021-06-15 02:20:00', 'Ontime'),
(80, '12345671', 1, '2021-06-15 02:22:00', '1 week', 'Cancelled', '2021-06-22 02:22:00', 202100018, 0, 0, 0, '2021-06-22 00:00:00', 'Cancelled'),
(81, '1223', 1, '2021-06-15 11:18:00', '1 week', 'Returned', '2021-06-22 11:18:00', 202100018, 0, 0, 1, '2021-06-15 11:18:00', 'Ontime'),
(82, '12345678', 1, '2021-06-16 03:53:00', '1 week', 'Cancelled', '2021-06-23 03:53:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(84, '202110017', 1, '2021-06-16 09:22:00', '1 week', 'Returned', '2021-06-23 09:22:00', 202100018, 0, 0, 1, '2021-06-16 09:23:00', 'Ontime'),
(85, '202110034', 1, '2021-06-16 09:35:00', '1 week', 'Cancelled', '2021-06-23 09:35:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(86, '202110034', 1, '2021-06-16 09:36:00', '1 week', 'Returned', '2021-06-23 09:36:00', 202100018, 0, 0, 1, '2021-06-16 09:36:00', 'Ontime'),
(87, '202110016', 1, '2021-06-16 03:15:00', '1 week', 'Cancelled', '2021-06-23 03:15:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(88, '202110017', 1, '2021-06-16 03:31:00', '1 week', 'Returned', '2021-06-23 03:31:00', 202100018, 0, 0, 1, '2021-06-16 03:33:00', 'Ontime'),
(89, '202110015', 1, '2021-06-16 03:31:00', '1 week', 'Returned', '2021-06-23 03:31:00', 202100018, 0, 0, 1, '2021-06-16 03:33:00', 'Ontime'),
(90, '12345670', 1, '2021-06-16 03:32:00', '1 week', 'Cancelled', '2021-06-23 03:32:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(91, '202110017', 1, '2021-06-16 03:38:00', '1 week', 'Cancelled', '2021-06-23 03:38:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(92, '202110015', 1, '2021-06-16 03:38:00', '1 week', 'Cancelled', '2021-06-23 03:38:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled'),
(93, '202110016', 1, '2021-06-16 03:38:00', '1 week', 'Cancelled', '2021-06-23 03:38:00', 202100018, 0, 0, 0, '2021-06-23 00:00:00', 'Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `USERID` int(11) NOT NULL,
  `NAME` varchar(124) NOT NULL,
  `UEMAIL` varchar(125) NOT NULL,
  `PASS` varchar(125) NOT NULL,
  `TYPE` varchar(125) NOT NULL,
  `Status` varchar(11) NOT NULL,
  `PicLoc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`USERID`, `NAME`, `UEMAIL`, `PASS`, `TYPE`, `Status`, `PicLoc`) VALUES
(3, 'Richard Naing', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'Active', 'img/bhl-logo.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblauthors`
--
ALTER TABLE `tblauthors`
  ADD PRIMARY KEY (`AuthorId`);

--
-- Indexes for table `tblautonumbers`
--
ALTER TABLE `tblautonumbers`
  ADD PRIMARY KEY (`AUTOID`);

--
-- Indexes for table `tblbooknumber`
--
ALTER TABLE `tblbooknumber`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblbooks`
--
ALTER TABLE `tblbooks`
  ADD PRIMARY KEY (`BookID`),
  ADD UNIQUE KEY `IBSN` (`IBSN`);

--
-- Indexes for table `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tblinstructors`
--
ALTER TABLE `tblinstructors`
  ADD PRIMARY KEY (`IDNO`);

--
-- Indexes for table `tbllogs`
--
ALTER TABLE `tbllogs`
  ADD PRIMARY KEY (`LogId`);

--
-- Indexes for table `tblpayment`
--
ALTER TABLE `tblpayment`
  ADD PRIMARY KEY (`PaymentId`);

--
-- Indexes for table `tblprograms`
--
ALTER TABLE `tblprograms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblstudent`
--
ALTER TABLE `tblstudent`
  ADD PRIMARY KEY (`IDNO`),
  ADD UNIQUE KEY `BorrowerId` (`StudentId`);

--
-- Indexes for table `tbltransaction`
--
ALTER TABLE `tbltransaction`
  ADD PRIMARY KEY (`TransactionID`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`USERID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblautonumbers`
--
ALTER TABLE `tblautonumbers`
  MODIFY `AUTOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblbooknumber`
--
ALTER TABLE `tblbooknumber`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tblbooks`
--
ALTER TABLE `tblbooks`
  MODIFY `BookID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblinstructors`
--
ALTER TABLE `tblinstructors`
  MODIFY `IDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbllogs`
--
ALTER TABLE `tbllogs`
  MODIFY `LogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblpayment`
--
ALTER TABLE `tblpayment`
  MODIFY `PaymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tblstudent`
--
ALTER TABLE `tblstudent`
  MODIFY `IDNO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbltransaction`
--
ALTER TABLE `tbltransaction`
  MODIFY `TransactionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `USERID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
