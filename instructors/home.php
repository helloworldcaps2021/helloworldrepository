 <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                      <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">List of Books</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="books/index.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Students</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="Student/index.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Requested Books</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="requested/index.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Approved Books</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="approved/index.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header"><i class="fas fa-table mr-1"></i>Inventory of Books</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Book Title</th>
                                                <th>Description</th>
                                                <th>Category</th>
                                              <!--  <th>Quantity</th> -->
                                            </tr>
                                        </thead> 
                                        <tbody>

                                            <?php 
                                                     $mydb->setQuery("SELECT *, sum(BookQuantity) as qty FROM `tblbooks`  GROUP BY BookTitle");   
                                                    $cur = $mydb->loadResultlist();
                                                    foreach ($cur as $result) {
                                                        echo '<tr>';  
                                                        echo '<td >'. $result->BookTitle.'</td>';
                                                        echo '<td>'.  $result->BookDesc.'</td>'; 
                                                        echo '<td>'. $result->Category.'</td>';  
                                                       // echo '<td>'. $result->qty.'</td>';


                                                        echo '</tr>';

                                                  
                                                }  
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card mb-4" style="" >
                                    <div class="card-header"><i class="fas fa-chart-area mr-1"></i>Transaction History</div>
                                    <div class="card-body">
                                        <div class="table-responsive"> 
 
                                                <table id="dataTable" class="table table-bordered table-hover" cellspacing="0"  >
                                                
                                                  <thead>
                                                    <tr> 
                                                        <th>Reference ID</th>
                                                        <th>Title</th> 
                                                        <th>Student</th> 
                                                        <th>DateRequested</th>
                                                      <!--  <th>DueDate</th> -->
                                                        <th>Date Approved</th>
                                                        <th>Status</th> 
                                                    </tr>   
                                                  </thead>
                                                  <tbody>
                                                    <?php 
                                                   $mydb->setQuery("SELECT * FROM `tblbooks` b, `tbltransaction` t ,`tblstudent` s, `tblinstructors` i, `tblcategory` c  
                                    WHERE b.IBSN=t.IBSN AND t.StudentId=s.StudentId AND b.Category=c.Category AND c.DDecimal=i.Category  AND i.IDNO = ".$_SESSION['IDNO']); 
                                                            $cur = $mydb->loadResultlist();
                                                            foreach ($cur as $result) {
                                                                echo '<tr>';  
                                                                echo '<td ><a href="'.web_root.'instructors/requested/index.php?view=view&id='.$result->TransactionID.'">' . $result->IBSN.'</a></td>';
                                                                echo '<td >'. $result->BookTitle.'</td>'; 
                                                                echo '<td>'. $result->Firstname.' '. $result->MiddleName.' '. $result->Lastname.'</td>';
                                                                echo '<td>'. $result->DateBorrowed.'</td>';
                                                              //  echo '<td>'. $result->DueDate.'</td>'; 
                                                                echo '<td>'. $result->DateReturned.'</td>';
                                                                
                                                                if ($result->Remarks=='Ontime') {
                                                                # code...
                                                                    echo '<td align="center" >     
                                                                     <a title="Approved"> Approved</a>
                                                                     </td>';
                                                              }else{ echo '<td align="center" >     
                                                                     <a title="Approved"> Cancelled</a>
                                                                     </td>'; }
                                                                 
                                                                echo '</tr>';
                                                              
                                                            } 
                                                    ?>
                                                  </tbody>
                                                
                                                </table>

                                         
                             
                                            </div>

                                    </div>
                                </div>
                            </div>
                           

                                    </div> 
                       