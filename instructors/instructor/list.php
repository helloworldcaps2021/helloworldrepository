<?php
	 if (!isset($_SESSION['ITYPE'])=='Instructor'){
      redirect(web_root."index.php");
     }

?>   

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List of Instructors <a href="index.php?view=add" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i> New</a></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0"  style="white-space: nowrap;">
				  <thead> 
				  		<th> First Name</th>
				  	
				  		<th>Email</th>
				  		<th>Type</th>
				  		<th width="20%">Action</th>  
				  </thead> 
				  <tbody>
				  	<?php 
				  		// $mydb->setQuery("SELECT * 
								// 			FROM  `tblusers` WHERE TYPE != 'Customer'");
				  		$mydb->setQuery("SELECT * 
											FROM  `tblinstructors`");
				  		$cur = $mydb->loadResultList();

						foreach ($cur as $result) {
				  		echo '<tr>'; 
				  		echo '<td>' . $result->FNAME.' ' . $result->MNAME.' ' . $result->LNAME.'</a></td>';
				  	
				  		echo '<td>'. $result->EMAIL.'</td>';
				  		echo '<td>'. $result->ITYPE.'</td>';

				  		if ($result->ITYPE=="Instructor") {
				  			# code...
				  			$btn = '<a title="Edit" href="index.php?view=edit&id='.$result->IDNO.'" class="btn btn-primary btn-sm  " ><i class="fa fa-edit"></i> Edit</a> <a title="Edit" href="index.php?view=changepassword&id='.$result->IDNO.'" class="btn btn-success btn-sm  " ><i class="fa fa-lock"></i> Change Passoword</a> <a title="View" href="index.php?view=view&id='.$result->IDNO.'" class="btn btn-info btn-sm  " ><i class="fa fa-edit"></i> View</a>
				  			 <a title="Delete" href="controller.php?action=delete&id='.$result->IDNO.'" class="btn btn-danger btn-sm  " ><i class="fa fa-trash"></i> Delete</a>';

				  		}else{
				  			$btn= '<a title="Edit" href="index.php?view=edit&id='.$result->IDNO.'" class="btn btn-primary btn-sm  " ><i class="fa fa-edit"></i> Edit</a> <a title="Edit" href="index.php?view=changepassword&id='.$result->IDNO.'" class="btn btn-success btn-sm  " ><i class="fa fa-lock"></i> Change Passoword</a> <a title="View" href="index.php?view=view&id='.$result->IDNO.'" class="btn btn-info btn-sm  " ><i class="fa fa-edit"></i> View</a>
				  					 <a title="Delete" href="controller.php?action=delete&id='.$result->IDNO.'" class="btn btn-danger btn-sm  " ><i class="fa fa-trash"></i> Delete</a>';
				  		}
				  		echo '<td > '.$btn.'</td>';
				  		echo '</tr>';
				  	} 
				  	?>
				  </tbody>
					
				</table> 
			</div>
		</div>   