 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>History and Library - free css website template</title>
<meta name="keywords" content="History and Library, free css website template, CSS, HTML" />
<meta name="description" content="History and Library - free css website template by templatemo.com" /> 
 
        <link rel="stylesheet" href="<?php echo web_root;?>asset/font-awesome/css/font-awesome.min.css" type="text/css">
<link href="<?php echo web_root;?>style.css" rel="stylesheet" type="text/css" />

   <style type="text/css">
       li {
        list-style: none;
       }
   </style>
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
</script>
</head>
<body>
<div id="templatemo_container">
    
   
    
    <div id="templatemo_content_mid">

        <?php require_once($content);?>
        
      
        
      </div>
        <script src="<?php echo web_root;?>asset/js/jquery-2.1.3.min.js"></script>

        <script src="<?php echo web_root;?>asset/bootstrap/js/bootstrap.min.js"></script>
</html>

   