<?php 
 if (!isset($_SESSION['USERID'])){
    redirect(web_root."admin/index.php");
   }

  $autonumber = New Autonumber();
  $auto = $autonumber->set_autonumber("IBSN"); 
 ?> 


        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Add New Thesis</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">


<form class="form-horizontal well span4" action="controller.php?action=add" method="POST" autocomplete="off" enctype="multipart/form-data">

   <div class="card-header">
                       

                           
 <div class="col-md-12"> 
    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="IBSN">Reference ID</label><input class="form-control " id="IBSN" name="IBSN" type="text" placeholder="Enter Reference Id"  ></div> <!--value="<?php echo DATE('Y').$auto->AUTO; ?>" readonly-->
        </div> 
    </div>
    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="BookTitle">Title</label><input class="form-control " id="BookTitle" name="BookTitle" type="text" placeholder="Enter Title" /></div>
        </div>
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="BookDesc">Abstract</label><textarea class="form-control " id="BookDesc" name="BookDesc" type="text" placeholder="Enter Abstract" ></textarea></div>
        </div>

    </div>  
    <a href="javascript:void(0)" class="add-more-form  btn btn-primary"style=" height: 40px;">ADD MORE</a>
    <div class="form-row">
        

 
         <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="Author">Author</label>
            <input class="form-control " id="Author" name="Author" type="text" placeholder="Enter Author" />
          </div>
        </div>
         <div class="paste-new-forms"></div>

      

        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="inputConfirmPassword">Published Date</label>
             <div class='input-group date'>
                    <input type='text' class="form-control" name="PublishDate" id="datepicker" placeholder="Select Date" readonly="false" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
          </div>
        </div>
    </div>  
    <div class="form-row">
       
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="Category">Category</label>
                 <select class="form-control " name="Category" id="Category">
                      <?php
                      $categ = new Category();
                      $cur = $categ->listofcategory(); 
                      foreach ($cur as $category) {
                        echo '<option>'.$category->Category.' </option>';
                      }

                   ?>
              
            </select> 
          </div>
        </div>

     
    </div> 
    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="DDecimal">Dewey Decimal</label><input class="form-control " id="DDecimal" name="DDecimal" type="text" placeholder="Enter Dewey Decimal" readonly="true" /></div>
        </div>
       <!-- <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="inputConfirmPassword">Type</label> 
             <select class="form-control input-sm" name="BookType" id="BookType">
              <option>Fiction</option>
              <option>Non-Fiction</option>  
              <option>Unknown</option>  
          </select> </div>
        </div>
    </div>   
    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="BookPrice">Price</label><input class="form-control " id="BookPrice" name="BookPrice" type="text" placeholder="Enter Price" /></div>
        </div> -->
        <div class="col-md-6">
            <div class="form-group"><label class="small mb-1" for="Remarks">Remarks</label><input class="form-control " id="Remarks" name="Remarks" type="text" aria-describedby="emailHelp" placeholder="Enter Remarks" /></div>
        </div>

              <div class="form-row">
             <div class="col-md-6">
                <label class="small mb-1">File Upload</label>
                 <input type="file" class="form-control" id="fileupload" name="fileupload"  >
                                        
            </div>
        </div> 
                            

                        
    </div>   
</div>

       <div class="form-group">
              <div class="col-md-8">
                <label class="col-md-4 control-label" for=
                "idno"></label>

                <div class="col-md-8">
                  <button class="btn btn-primary" name="savecourse" type="submit" ><span class="fa fa-save"></span> Save</button>
               <!--   <button class="btn btn-primary" name="saveandnewcourse" type="submit" ><span class="fa fa-save"></span> Save and Add new</button> -->
                </div>
              </div>
            </div>

         
            
  </form> 

</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
<script>
        $(document).ready(function () {
             $(document).on('click', '.remove-btn', function () {
                $(this).closest('.main-form').remove();
            });
            $(document).on('click', '.add-more-form', function () {
                $('.paste-new-forms').append('<div class="main-form mt-3 border-bottom">\
                                <div class="row">\
                                     <div class="col-md-6">\
            <div class="form-group"><label class="small mb-1" for="Author">Author</label>\
            <input class="form-control " id="Author" name="Author" type="text" placeholder="Enter Author" />\
          </div>\
        </div>\
                                    <div class="col-md-4">\
                                        <div class="form-group mb-2">\
                                            <br>\
                                            <button type="button" class="remove-btn btn btn-danger">Remove</button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>');
            });

        });
    </script>