<?php
   if (!isset($_SESSION['USERID'])){
      redirect(web_root."admin/index.php");
     }
if(!$_SESSION['TYPE']=='Administrator'){
  redirect(web_root."admin/index.php");
}
		check_message(); 
		?> 

       
 
  <section class="content">
   <div class="container-fluid">     
		 <?php
$connect = new PDO("mysql:host=localhost;dbname=lnuthesisrepodb", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'database_backup_lnurepodb' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>

  

  <div class="card">
   <div class="row">
    
    <br />
    <form method="post" id="export_form">
     <h3>Select Tables for Export</h3>
    <?php
    foreach($result as $table)
    {
    ?>
     <div class="demo-checkbox">
      <input type="checkbox" class="filled-in chk-col-indigo" name="table[]" value="<?php echo $table["Tables_in_lnuthesisrepodb"]; ?>" checked/> <label><?php echo $table["Tables_in_lnuthesisrepodb"]; ?></label>
     </div>


    
    <?php
    }
    ?>
     <div class="form-group">
      <input type="submit" name="submit" id="submit" class="btn btn-info" value="Export" />
     </div>
    </form>
   </div>
  </div>
 
</div>
    </section>
                     

     
   
<script>
$(document).ready(function(){
 $('#submit').click(function(){
  var count = 0;
  $('.filled-in chk-col-indigo').each(function(){
   if($(this).is(':checked'))
   {
    count = count + 1;
   }
  });
  if(count > 0)
  {
   $('#export_form').submit();
  }
  else
  {
   alert("Please Select Atleast one table for Export");
   return false;
  }
 });
});
</script>
        
           <!-- Jquery Core Js -->
    <script src="../template/plugins/jquery/jquery.min.js"></script>

   

    <!-- Select Plugin Js -->
    <script src="../template/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../template/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../template/plugins/node-waves/waves.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="../template/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="../template/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../template/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="../template/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Custom Js -->
    <script src="../template/js/admin.js"></script>
    <script src="../template/js/pages/forms/basic-form-elements.js"></script>

    <!-- Demo Js -->
    <script src="../template/js/demo.js"></script>