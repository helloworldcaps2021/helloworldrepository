<?php
require_once("../include/initialize.php");

 ?>
  <?php
 // login confirmation
  if(isset($_SESSION['USERID'])){
    redirect(web_root."admin/index.php");
  }
  ?> 





<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>LNU Thesis Repository</title>
    <link rel="shortcut icon" href="../assets/images/fav.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="../assets/images/fav.jpg">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/all.min.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/plugins/slider/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../assets/plugins/slider/css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css" />
</head>

    <body class="form-login-body"> 
              <div class="container">
                            <form class="form-horizontal span4" action="" method="POST" autocomplete="off">
                <div class="row">
                    <div class="col-lg-9 mx-auto login-desk">
                       <div class="row">
                           
                            <div class="col-md-5 loginform" >
                                <div class="log-txt row no-margin">
                                    <h2>Leyte Normal University Thesis  Repository Admin
                                    Login Page  </h2>
                                    <br><br>
                                    
                                </div>
                                
                                 <div class="login-det">
                                    <img src="../assets/images/gadgets.png" alt="">
                                 </div>
                            </div>
                             <div class="col-md-7 detail-box">
                               
                                <div class="detailsh col-lg-7 col-md-10 col-sm-7 col-11 mx-auto">
                                      <img class="logo" src="../assets/images/lnu_logo.png" alt=""> 
                                      
                                      <div class="row form-ro no-margin" id="Username">
                                          <input type="text" id="inputEmailAddress" name="user_email" type="input" placeholder="Enter Username"class="form-control form-control-sm">
                                      </div>
                                      
                                       <div class="row form-ro no-margin" id="password">
                                          <input type="password" id="inputPassword" name="user_pass" type="password" placeholder="Enter password" class="form-control form-control-sm">
                                      </div>
                                    <div class="row form-ro fgbh">
                                        <div class="col-6">
                                            <a href="">Forget Password</a>
                                        </div>
                                        <div class="col-6" id="subjcode">
                                            <button class="btn btn-sm btn-primary" type="submit" name="btnLogin" value="Login">Sign In</button>
                                        </div>
                                    </div>
                                    <div class="row form-ro hlio fgbh">
                                        
                                        
                                    </div>
                                </div>
                            </div>
                       </div>
                      
                    </div>
                </div>
            </form>
            </div>
    </body>

    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/scroll-fixed/jquery-scrolltofixed-min.js"></script>
    <script src="../assets/plugins/slider/js/owl.carousel.min.js"></script>
    <script src="../assets/js/script.js"></script>
</html>




 
<?php

if(isset($_POST['btnLogin'])){
  $email = trim($_POST['user_email']);
  $upass  = trim($_POST['user_pass']);
  $h_upass = sha1($upass);

   if ($email == '' OR $upass == '') {

      message("Invalid Username and Password!", "error");
      redirect("login.php");

    } else {
  //it creates a new objects of member
    $user = new User();
    //make use of the static function, and we passed to parameters
    $res = $user::userAuthentication($email, $h_upass);
    if ($res==true) {
       message("You login as ".$_SESSION['TYPE'].".","success");
      if ($_SESSION['TYPE']=='Administrator'){
         redirect(web_root."admin/index.php");
      }else{
           redirect(web_root."admin/login.php");
      }
    }else{
      message("Account does not exist! Please contact Administrator.", "error");
       redirect(web_root."admin/login.php");
    }
 }
 }
 ?> 