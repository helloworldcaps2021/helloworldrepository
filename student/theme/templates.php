 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LNU Thesis Repo</title>
<meta name="keywords" content="History and Library, free css website template, CSS, HTML" />
<meta name="description" content="History and Library - free css website template by templatemo.com" /> 
 
        <link rel="stylesheet" href="<?php echo web_root;?>asset/font-awesome/css/font-awesome.min.css" type="text/css">
<link href="<?php echo web_root;?>style.css" rel="stylesheet" type="text/css" />

   <style type="text/css">
       li {
        list-style: none;
       }
   </style>

   <style>


.dropdown {
  float: right;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: #01185F;
  padding-right: 20px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
  margin-right: 50px;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 12px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
</script>
</head>
<body>
<div id="templatemo_container">
    <div id="templatemo_banner">
   
        <img src="../images/lnu_logo.png" style=" padding-top: 8px; padding-left: 50px; padding-right: 10px;"align="left"width="75" height="75" alt="LNU Logo" href="">
                </img>
                <p class="navbar-brand" style="padding-left: 30px; padding-top: 37px; margin-left: 10px;  font-weight: bold; font-size: 26px; color: #01185F;" href="">LNU THESIS REPOSITORY
            </p>

            
                

  <div class="dropdown" style="float: right; ">
    <button class="dropbtn"> 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a  href="<?php echo web_root;?>logout.php">Logout</a>
      
    </div>
  </div> 


        
                  
            
    </div> <!-- end of banner -->
    
    <div id="templatemo_menu">
        <ul>
            <
            <li><a href="<?php echo web_root;?>student/index.php">Home</a></li> 
            <li><a href="<?php echo web_root;?>student/index.php?q=books">Thesis</a></li> 
            <li><a href="<?php echo web_root;?>student/index.php?q=about">About Us</a></li>
            <li><a href="<?php echo web_root;?>student/index.php?q=contact" class="last">Contact US</a></li>
            <li><a href="<?php echo web_root;?>student/index.php?q=profile">Profile</a></li>
            
            
        </ul>       
    </div> <!-- end of menu -->
    
    <div id="templatemo_content_top">

<div id="sidebar">
    <!--  Free CSS Templates @ www.TemplateMo.com  -->
        <form action="<?php echo web_root;?>student/index.php?q=find"class="form-horizontal span4" action="#.php" method="POST" autocomplete="off">

                  
                      <div class="panel-body">

                        <div class="form-group" id="BookTitle">
                            <div class="row">
                              <label class="col-md-2 control-label" for=
                              "BookTitle">Title:</label>

                              <div class="col-md-10">
                                 <input class="form-control input-sm" id="BookTitle" name="BookTitle" placeholder=
                                                      "Title" type="text" value="">
                              </div>

                            </div> 
                          </div>
                          <div class="form-group" id="Category">
                            <div class="row">
                             <label class="col-md-2 control-label" for=
                                "Category">Category:</label>

                                <div class="col-md-10">
                                 <select class="form-control input-sm" name="Category" id="Category" style="height: 42px; width: 273px;" >
                                    <option value="" >Select Book Category</option>
                                    <?php
                                    $category = new Category();
                                    $cur = $category->listOfcategory(); 
                                    foreach ($cur as $category) {
                                        echo '<option>'.$category->Category .'</option>';
                                    }

                                    ?>
                                        
                                    </select>   
                                </div>

                            </div>

                          </div>
                          <div class="form-group" id="Author">
                            <div class="row">
                               <label class="col-md-2 control-label" for=
                                "Author">Author:</label>

                                <div class="col-md-10">
                                  <input class="form-control input-sm" id="Author" name="Author" type=
                                  "text" placeholder="Author">
                                </div>

                            </div>

                          </div>
                           <div class="form-group" id="BookPublisher">
                            <div class="row">
                               <label class="col-md-2 control-label" for=
                                "BookPublisher">Grammarian:</label>

                                <div class="col-md-10">
                                  <input class="form-control input-sm" id="BookPublisher" name="BookPublisher" type=
                                  "text" placeholder="Grammarian">
                                </div>

                            </div>

                          </div>
                           <div class="form-group" id="PublishDate">
                            <div class="row">
                               <label class="col-md-2 control-label" for=
                                "PublishDate">Date:</label>

                                <div class="col-md-10">
                                  <input class="form-control input-sm datepicker" id="datepicker" name="PublishDate" type=
                                  "text" placeholder="Published Date"   data-inputmask="'alias': 'date'">
                                </div>

                            </div>

                          </div>

                        <div class="form-group" id="subjcode">
                            <div class="row">
                               <label class="col-md-2 control-label"></label>

                                <div class="col-md-10">
                                     <div class="btn-group" align="center"style="margin-top: 10px; margin-left: 20px; margin-bottom: 10px;">
                                        <button  href="index.php?q=find" type="submit" name="search" class="btn btn-primary"><i class="fa fa-search" ></i> Search</button>
                                        
                                         <a href="index.php?q=find#.php" class="btn btn-success  " style="padding: 6px 25px 6px 20px;"><i class="fa fa-arrow-left"></i> Back</a> 
                                                              
                                    </div>
                                </div>

                            </div>

                          </div>

                    
                    </div>
                                    
                </form>
      <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
       
         <!-- Start Blog categories widget -->
                   
                        
                        
                            <h4 class="section-title" style="margin-left: 20px; margin-top: 40px;" >
                                <span>BOOK CATEGORIES</span>
                            </h4>
                       
                        <style type="text/css">
                            .selected{
                                color: #FF432E;
                            }
                        </style>
                        <ul>
                             <?php
                                $category = new Category();
                                $cur = $category->listOfcategory(); 
                                foreach ($cur as $category) { 
                                    if ($category==$category->Category) {
                                        # code...
                                         echo ' <li style="color:#FF432E">
                                                <i class="fa fa-angle-double-right"></i>
                                                <a  style="color:#FF432E" href="index.php?q=books&category='.$category->Category.'">'.$category->Category .'</a> 
                                            </li> ';
                                    }else{
                                             echo '<li>
                                                <i class="fa fa-angle-double-right"></i>
                                                <a   href="index.php?q=books&category='.$category->Category.'">'.$category->Category .'</a> 
                                            </li> ';
                                    }
                                   
                                }
                            ?>
                           
                        </ul>
                        
                 
    </div>
         


        <?php require_once($content);?>
        
      
        
        <div class="margin_bottom_40 b_bottom"></div>
        <div class="margin_bottom_30"></div>
        
        <div id="templatemo_footer">
        
            <ul class="footer_list">
            <li><a href="index.php">Home</a></li>
            <li><a href="<?php echo web_root;?>index.php?q=books">Thesis</a></li> 
            <li><a href="<?php echo web_root;?>index.php?q=about">About Us</a></li>
            <li><a href="<?php echo web_root;?>index.php?q=contact" class="last">Contact US</a></li>
            </ul> 
            
            <div class="margin_bottom_10"></div>
            
            Copyright © 2021 <a href="">Leyte Normal University</a> | <a href="" target="_parent">Thesis Repository</a> | <a href="">HelloWorld2.0</a>
            
            <div class="margin_bottom_20"></div>
             
        </div>
        
        <div class="cleaner"></div>    
    </div> <!-- end of content bottom -->
</div> <!-- end of container -->
<!--  Free Website Templates @ TemplateMo.com  -->


        <script src="<?php echo web_root;?>asset/js/jquery-2.1.3.min.js"></script>

        <script src="<?php echo web_root;?>asset/bootstrap/js/bootstrap.min.js"></script>
</html>

   